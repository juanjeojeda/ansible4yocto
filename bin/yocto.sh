#!/bin/sh

cd ~/ansible4yocto

ansible-playbook  -i  inventories/yocto.ini playbooks/yocto-install.yaml
ansible-playbook  -i  inventories/yocto.ini   playbooks/yocto.yaml
